import { Effect, Reducer, Subscription, request } from 'umi';
import herolistjson from '../../mock/herolist.json';

export interface HeroProps {
    ename: number;
    cname: string;
    title: string;
    new_type: number;
    hero_type: number;
    skin_name: string;
}

// bean类
export interface HeroModelState {
    name: string;
    heros: HeroProps[];
    filterKey: number,
    freeheros: HeroProps[]
    itemHover: number
}

export interface HeroModelType {
    namespace: 'hero';
    state: HeroModelState;
    effects: {
        query: Effect;
        fetch: Effect
    };
    reducers: {
        save: Reducer<HeroModelState>;
    };
    subscriptions: {
        setup: Subscription
    }
}

// 模型
const HeroModel: HeroModelType = {
    namespace: 'hero',

    state: {
        name: 'hero',
        heros: [],
        filterKey: 0,
        freeheros: [],
        itemHover: 0
    },

    effects: {
        *query({ payload }, { call, put }) {

        },

        *fetch({ type, payload }, { put, call, select }) {
            const localData = [
                {
                    ename: 105,
                    cname: '廉颇',
                    title: '正义爆轰',
                    new_type: 0,
                    hero_type: 3,
                    skin_name: '正义爆轰|地狱岩魂',
                },
                {
                    ename: 106,
                    cname: '小乔',
                    title: '恋之微风',
                    new_type: 0,
                    hero_type: 2,
                    skin_name: '恋之微风|万圣前夜|天鹅之梦|纯白花嫁|缤纷独角兽',
                },
            ]

            // const freeheros = yield request('/freeheros.json', {
            //     method: 'POST',
            //     headers: {
            //         Accept: 'application/json',
            //         'Content-Type': 'application/json; charset=utf-8'
            //     },
            //     body: JSON.stringify({
            //         number: 10
            //     })
            // })
            
            const herolist = herolistjson;
            function getRandomArrayElements(arr, count) {
                var shuffled = arr.slice(0),
                    i = arr.length,
                    min = i - count,
                    temp,
                    index;
                while (i-- > min) {
                    index = Math.floor((i + 1) * Math.random());
                    temp = shuffled[index];
                    shuffled[index] = shuffled[i];
                    shuffled[i] = temp;
                }
                return shuffled.slice(min);
            }
            const freeheros = getRandomArrayElements(herolistjson, 10);

            // const data = yield request('/web201605/js/herolist.json')

            // const data = yield request('/herodetails.json', {
            //     method: 'POST',
            //     headers: {
            //         Accept: 'application/json',
            //         'Content-Type': 'application/json; charset=utf-8'
            //     },
            //     body: JSON.stringify({
            //         ename: 110
            //     })
            // });
            yield put({
                type: 'save',
                payload: {
                    // heros: data || localData,
                    heros: herolist,
                    freeheros
                }
            })
        }
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    },
    subscriptions: {
        setup({ dispatch, history }) {
            return history.listen(({ pathname, query }) => {
                if (pathname === '/hero') {
                    dispatch({
                        type: 'fetch'
                    })
                }
            })
        }
    }
};

export default HeroModel;