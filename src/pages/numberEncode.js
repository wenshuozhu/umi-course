import { Button, message, Select, InputNumber, Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import styles from './numberEncode.less';

const { Option } = Select;

export default function Page() {
  const [number, setNumber] = useState(0);
  const [maxnumber, setMaxnumber] = useState(99);
  const [start, setStart] = useState(false);
  const [week, setWeek] = useState(1);
  const [twonum, setTwonum] = useState(false);

  useEffect(() => {
    if (start) {
      if (number >= maxnumber) {
        // message.success('结束')
        alert('结束');
        setNumber(0);
        setStart(false);
      } else {
        addNumber();
      }
    }
  });

  const addNumber = () => {
    setTimeout(() => {
      setNumber(number + 1);
    }, week * 1000);
  };

  const toggle = () => {
    setStart(!start);
  };

  const handleChange = value => {
    setWeek(value);
  };

  const onChangeFrom = value => {
    setNumber(value);
  };

  const onChangeTo = value => {
    setMaxnumber(value);
  };

  const onChangeTwonum = checked => {
    setTwonum(checked);
  };

  return (
    <div>
      {/* <h1 className={styles.title}>Page numberEncode</h1> */}
      <div>
        <Button type={start ? 'danger' : 'primary'} onClick={toggle}>
          {start ? '暂停' : '开始'}
        </Button>

        <div style={{ marginTop: '20px' }}>
          <span>周期</span>
          <Select
            defaultValue={week}
            style={{ width: 120 }}
            onChange={handleChange}
            placeholder="周期"
          >
            <Option value="1">1</Option>
            <Option value="2">2</Option>
            <Option value="3">3</Option>
          </Select>
        </div>

        <div style={{ marginTop: '20px' }}>
          <span>从</span>
          <InputNumber
            min={0}
            max={99}
            defaultValue={number}
            onChange={onChangeFrom}
          />
          <span style={{ marginLeft: '20px' }}>到</span>
          <InputNumber
            min={0}
            max={99}
            defaultValue={maxnumber}
            onChange={onChangeTo}
          />
        </div>

        <div style={{ marginTop: '20px' }}>
          <Switch
            checkedChildren="开启"
            unCheckedChildren="关闭"
            defaultChecked={twonum}
            onChange={onChangeTwonum}
          />
        </div>
      </div>
      <div className={styles.number}>
        {twonum ? (number < 10 ? '0' + number : number) : number}
      </div>
    </div>
  );
}
