import React, { useState } from 'react';
import styles from './dromlife.less';
import { Table, Tag, Radio, Divider } from 'antd';

const columns = [
  {
    title: '业务',
    dataIndex: 'business',
    key: 'business',
  },
  {
    title: '办公时间',
    dataIndex: 'time',
    key: 'time',
  },
  {
    title: '办公地点',
    dataIndex: 'address',
    key: 'address',
    render: (addresses: string[]) => (
      <>
        {addresses.map(address => {
          return (
            <li style={{ marginTop: 5 }}>
              <Tag key={address} color="green">
                {address}
              </Tag>
            </li>
          );
        })}
      </>
    ),
  },
  {
    title: '业务人员',
    dataIndex: 'worker',
    key: 'worker',
  },
  {
    title: '联系方式',
    dataIndex: 'phones',
    key: 'phones',
    render: (phones: string[]) => (
      <>
        {phones.map(phone => {
          let color = phone.length > 11 ? 'geekblue' : 'green';
          return (
            <li style={{ marginTop: 5 }}>
              <Tag key={phone} color={color}>
                {phone}
              </Tag>
            </li>
          );
        })}
      </>
    ),
  },
  {
    title: '其它',
    dataIndex: 'other',
    key: 'other',
  },
];

const dataSource = [
  {
    key: '1',
    business: '移动无线维护',
    time: '',
    address: [''],
    worker: '',
    phones: ['15195812665'],
    other: '',
  },
  {
    key: '2',
    business: '电信有线维护',
    time: '',
    address: [''],
    worker: '',
    phones: ['18951075573'],
    other: '',
  },
  {
    key: '3',
    business: '热水器维修',
    time: '',
    address: [''],
    worker: '',
    phones: ['025—85641771', '025—85328868', '编辑短信发送至18013006669'],
    other: '',
  },
  {
    key: '4',
    business: '新装热水器维修',
    time: '',
    address: [''],
    worker: '蔡',
    phones: ['13305106469'],
    other: '',
  },
  {
    key: '5',
    business: '空调租赁维修',
    time: '',
    address: [''],
    worker: '',
    phones: ['18251918410'],
    other: '',
  },
  {
    key: '6',
    business: '水、电、木维修',
    time: '',
    address: [''],
    worker: '',
    phones: [''],
    other: '微信报修',
  },
  {
    key: '7',
    business: '洗衣机维护',
    time: '',
    address: [''],
    worker: '',
    phones: [
      '13851581343（20#—24#，27#4、5层）',
      '13776454776（庄欢）（15#—19#，25#26#27#1—3层）',
    ],
    other: '',
  },
  {
    key: '8',
    business: '充电',
    time: '9：00—16：30 （星期一～星期五）',
    address: ['象山苑门口', '西苑小二楼', '东苑食堂一楼', '浦江热水站'],
    worker: '',
    phones: [''],
    other: '',
  },
  {
    key: '9-1',
    business: '水卡充值',
    time: '每周一11：30——13：30',
    address: ['东苑4栋门卫值班室'],
    worker: '',
    phones: [''],
    other: '',
  },
  {
    key: '9-2',
    business: '水卡充值',
    time: '每周三11：00——13：00',
    address: ['亚青25栋 门卫值班室'],
    worker: '',
    phones: [''],
    other: '',
  },
  {
    key: '9-3',
    business: '水卡充值',
    time: '每周四11：00——14：00',
    address: ['新南苑食堂门口'],
    worker: '',
    phones: [''],
    other: '',
  },
  {
    key: '9-4',
    business: '水卡充值',
    time: '每周一14：00—16：00',
    address: ['西苑A栋门卫值班'],
    worker: '',
    phones: [''],
    other: '',
  },
  {
    key: '9-5',
    business: '水卡充值',
    time: '每周三14：00——16：00',
    address: ['檀香门卫值班室'],
    worker: '',
    phones: [''],
    other: '',
  },
];

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      'selectedRows: ',
      selectedRows,
    );
  },
  getCheckboxProps: record => ({
    disabled: record.business === '移动无线维护',
    business: record.business,
  }),
};

export default () => {
  const [selectionType, setSelectionType] = useState('checkbox');

  return (
    <div>
      <h1 className={styles.title}></h1>
      <div>
        <Radio.Group
          onChange={({ target: { value } }) => {
            setSelectionType(value);
          }}
          value={selectionType}
        >
          <Radio value="checkbox">checkbox</Radio>
          <Radio value="radio">radio</Radio>
        </Radio.Group>
      </div>
      <Divider />
      <Table
        rowSelection={{
          type: selectionType,
          ...rowSelection,
        }}
        dataSource={dataSource}
        columns={columns}
      />
    </div>
  );
};
