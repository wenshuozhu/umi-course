import React from 'react';
import styles from './engineer-ethics.less';
import {Typography, Divider} from 'antd'
const {Title, Paragraph, Link} = Typography
import {SmileOutlined, SmileFilled} from '@ant-design/icons'

export default () => {
  return (
    <div>      
      <Typography>
        <Title level={2} className={styles.title} copyable={{icon: [<SmileOutlined key="copy-icon"/>, <SmileFilled key="copied-icon"/>]}}>几代微处理器的技术特点（1000字以内）</Title>
        <Paragraph copyable>自1981年美国IBM公司推出第一代微型计算机IBM-PC/XT以来，微型机以其执行结果精确、处理速度快捷、性价比高、轻便小巧等特点迅速进入社会各个领域，且技术不断更新、产品快速换代，从单纯的计算工具发展成为能够处理数字、符号、文字、语言、图形、图像、音频、视频等多种信息的强大多媒体工具。</Paragraph>
        <Paragraph copyable>自第一台微型计算机MCS-4诞生后，微型计算机的发展非常迅速！对于微型计算机的发展，一般以字长和典型的微处理器芯片作为划分标志，将微型计算机的发展划分为五个阶段。</Paragraph>
        <Paragraph copyable>第一个阶段（1971~1973），主要是字长为4位的微型机和字长为8位的低档微型机。这一阶段的典型微处理器有：世界上第一个微处理器芯片4004，以及随后的改进版4040，它们都是字长为4位的。在随后的第二年，Intel又研制出了字长为8位的处理器芯片8008，集成度和性能都有所提高。8008采用PMOS工艺，字长8位，基本指令48条，基本指令周期为20~50uS，时钟频率为500KHz，集成度约为3500晶体管每片。</Paragraph>
        <Paragraph copyable>第二阶段（1973~1978），主要是字长为8位的中、高档微型机。这一阶段典型的微处理器芯片有：Intel公司的I8080、I8085、Motorola公司的M6800、Zilog公司的Z80等。以I8080为例，I8080采用NMOS工艺，字长8位，基本指令70多条，基本指令周期为2~10uS，时钟频率高于1MHz，集成度约为6000晶体管每片。</Paragraph>
        <Paragraph copyable>第三个阶段（1978~1985），主要是字长为16位的微型机。这一阶段典型的微处理器芯片有：Intel公司的8086/8088/80286、Motorola公司的M68000、Zilog公司的Z8000等。以8086为例，8086采用HMOS工艺，字长16位，基本指令周期为0.5uS，集成度约为2.9万晶体管每片。</Paragraph>
        <Paragraph copyable>第四个阶段（1985~2000），主要是字长为32位的微型机。这一阶段典型的微处理器芯片有：Intel公司的80386/486/Pentium/Pentium II/Pentium III/Pentium IV等。以I80386为例，其集成度达到27.5万晶体管每片，每秒钟可完成500万条指令（MIPS）。</Paragraph>
        <Paragraph copyable>在通用微处理器的研发领域，Intel公司一直处于领先位置。与此同时，作为它的竞争对手，AMD公司也先后推出了K5、K6、Duron、Athon等微处理器芯片。它们的共同特点是，都采用IA-32（Intel Architecture-32）指令架构，并逐步增加了面向多媒体数据处理和网络应用的扩展指令，如Intel的MMX、SSE等指令集和AMD的3DNow!等。一般将自8086以来一直延续的这种指令体系统称为x86指令体系。</Paragraph>
        <Paragraph copyable>我们目前使用的PC机，就是这一阶段的产品。</Paragraph>
        <Paragraph copyable>第五个阶段（2000~），出现了字长为64位的微处理器芯片。主要还是面向服务器和工作站等一些高端应用场合。如2000年Intel推出的微处理器Itanium（安腾），它采用全新指令架构IA-64。而AMD公司的64位微处理器Athlon 64则仍沿用了x86指令体系，能够很好的兼容原来的IA-32结构的个人微机系统，具有一定的普及性。</Paragraph>
        <Paragraph copyable>随着微型计算机的发展，在每一个阶段，它在集成度、性能等方面都有非常大的提高，微型计算机在今后将会有更快、更惊人的发展。</Paragraph>        
      </Typography>

      <Divider/>

      <Link href="http://www.docin.com/p-773538975.html" target="_blank">参考资料：美妙人生 -《微型计算机发展史》（备注：网络资源）</Link>
    </div>
  );
}
