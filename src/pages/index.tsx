import React from 'react';
import styles from './index.less';
import Animation from './canvas/animation1';

export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      realdate: new Date(),
    };
  }

  componentDidMount = () => {
    this.realdateTimer = setInterval(() => this.tick(), 1000);
  };

  componentWillUnmount = () => {
    clearInterval(this.realdateTimer);
  };

  tick = () => {
    this.setState({
      realdate: new Date(),
    });
  };

  render = () => {
    return (
      <div className={styles.root}>
        {/* <h1 className={styles.title}>Page index</h1> */}
        <h2 style={{ textAlign: 'right', color: '#ccc' }}>
          {this.state.realdate.toLocaleTimeString()}
        </h2>
        <div className={styles.animation}>
          <Animation />
        </div>
      </div>
    );
  };
}
