import React from 'react';
import styles from './row-selection-and-operation.less';
import {Table, Button} from 'antd'

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    filters: [
      {
        text: 'Joe',
        value: 'Joe',
      },
      {
        text: 'Jim',
        value: 'Jim',
      },
      {
        text: 'Submenu',
        value: 'Submenu',
        children: [
          {
            text: 'Green',
            value: 'Green',
          },
          {
            text: 'Black',
            value: 'Black'
          }
        ]
      }
    ],
    onFilter: (value, record) => record.name.indexOf(value) === 0,
    sorter: (a, b) => a.name.length - b.name.length,
    sortDirections: ['descend']
  },
  {
    title: 'Age',
    dataIndex: 'age',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.age - b.age,
  },
  {
    title: 'Address',
    dataIndex: 'address',
    filters: [
      {
        text: 'London',
        value: 'London',
      },
      {
        text: 'New York',
        value: 'New York',
      },
    ],
    filterMultiple: false,
    onFilter: (value, record) => record.address.indexOf(value) === 0,
    sorter: (a, b) => a.address.length - b.address.length,
    sortDirections: ['descend', 'ascend'],
  }
]

const data = []

for (let i = 0; i< 46; i++){
  data.push({
    key: i,
    name: `Edward King ${i}`,
    age: 32,
    address: `London, Park Lane no. ${i}`,
  })
}

export default class Index extends React.Component {
  state = {
    selectedRowKeys: [],
    loading: false,
  }  

  start = () => {
    this.setState({loading: true})
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false
      })
    },1000)
  }

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys)
    this.setState({
      selectedRowKeys
    })
  }

  render() {
    const {loading, selectedRowKeys} = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      selections: [
        Table.SELECTION_ALL,
        Table.SELECTION_INVERT,
        {
          key: 'odd',
          text: 'Select Odd Row',
          onSelect: changableRowKeys => {
            let newSelectedRowKeys = []
            newSelectedRowKeys = changableRowKeys.filter((key, index) => {
              if(index % 2 !== 0){
                return false
              }
              return true
            })
            this.setState({
              selectedRowKeys: newSelectedRowKeys
            })
          }
        },
        {
          key: 'even',
          text: 'Select Even Row',
          onSelect: changableRowKeys => {
            let newSelectedRowKeys = []
            newSelectedRowKeys = changableRowKeys.filter((key, index) => {
              if(index % 2 !== 0){
                return false
              }
              return true
            })
            this.setState({
              selectedRowKeys: newSelectedRowKeys
            })
          } 
        }
      ]
    }
    const hasSelected = selectedRowKeys.length > 0
    return (
      <div>
        <h1 className={styles.title}>Page mycomponents/table/row-selection-and-operation</h1>
        <div style={{marginBottom: 16}}>
          <Button type='primary' onClick={this.start} disabled={!hasSelected} loading={loading}>Reload</Button>
          <span style={{marginLeft: 8}}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Table rowSelection={rowSelection} columns={columns} dataSource={data}></Table>
      </div>
    );
  }
}
