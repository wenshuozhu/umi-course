import React from 'react';
import styles from './typography.less';
import {Typography, Divider} from 'antd'

const {Title, Paragraph, Text} = Typography

export default () => {
  return (
    <div>
      <h1 className={styles.title}>Page mycomponents/typography</h1>
      <Typography>
        <Title>This is title</Title>
        <Paragraph>
          This is Paragraph
        </Paragraph>
        <Paragraph>
          This is Paragraph
          <Text>
            This is text
          </Text>
          <Text strong>
            This is text with strong
          </Text>
        </Paragraph>
        <Title level={2}>This is level2 title</Title>
        <Paragraph>
          This is paragraph
          (<Text code>code1</Text> and <Text code>code2</Text>)
        </Paragraph>
        <Paragraph>
          This is paragraph
          Press <Text keyboard>Esc</Text> to exit...          
        </Paragraph>
        <Paragraph>
          <Text mark>This is text with mark</Text>
        </Paragraph>
        <Divider></Divider>

      </Typography>
    </div>
  );
}
