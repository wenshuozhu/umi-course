import React from 'react';
import styles from './animation1.css';
import { Button } from 'antd';
import RequestNextAnimationFrame from '@/utils/requestNextAnimationFrame';

export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      btn: 'Animate',
    };
    this.paused = true;
  }

  componentDidMount = () => {
    this.canvas = document.getElementById('canvas');
    this.mycontext = this.canvas.getContext('2d');

    this.mycontext.font = '48px Helvetica';

    this.discs = [
      {
        x: 150,
        y: 250,
        lastX: 150,
        lastY: 250,
        velocityX: -3.2,
        velocityY: 3.5,
        radius: 25,
        innerColor: 'rgba(255,255,0,1)',
        middleColor: 'rgba(255,255,0,0.7)',
        outerColor: 'rgba(255,255,0,0.5)',
        strokeStyle: 'gray',
      },

      {
        x: 50,
        y: 150,
        lastX: 50,
        lastY: 150,
        velocityX: 2.2,
        velocityY: 2.5,
        radius: 25,
        innerColor: 'rgba(100,145,230,1.0)',
        middleColor: 'rgba(100,145,230,0.7)',
        outerColor: 'rgba(100,145,230,0.5)',
        strokeStyle: 'blue',
      },

      {
        x: 150,
        y: 75,
        lastX: 150,
        lastY: 75,
        velocityX: 1.2,
        velocityY: 1.5,
        radius: 25,
        innerColor: 'rgba(255,0,0,1.0)',
        middleColor: 'rgba(255,0,0,0.7)',
        outerColor: 'rgba(255,0,0,0.5)',
        strokeStyle: 'orange',
      },
    ];
    this.numDiscs = this.discs.length;

    this.handleAnimate();
  };

  handleAnimate = () => {
    this.paused = this.paused ? false : true;
    if (this.paused) {
      this.setState({
        btn: 'Animate',
      });
    } else {
      RequestNextAnimationFrame(this.animate);
      this.setState({
        btn: 'Pause',
      });
    }
  };

  animate = time => {
    if (!this.paused) {
      this.mycontext.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.drawBackground();
      this.update();
      this.draw();
      RequestNextAnimationFrame(this.animate);
    }
  };

  drawBackground = () => {};

  update = () => {
    let disc = null;
    for (let i = 0; i < this.numDiscs; i++) {
      disc = this.discs[i];
      if (
        disc.x + disc.velocityX + disc.radius > this.mycontext.canvas.width ||
        disc.x + disc.velocityX - disc.radius < 0
      ) {
        disc.velocityX = -disc.velocityX;
      }
      if (
        disc.y + disc.velocityY + disc.radius > this.mycontext.canvas.height ||
        disc.y + disc.velocityY - disc.radius < 0
      ) {
        disc.velocityY = -disc.velocityY;
      }
      disc.x += disc.velocityX;
      disc.y += disc.velocityY;
    }
  };

  draw = () => {
    let disc = null;
    for (let i = 0; i < this.numDiscs; i++) {
      disc = this.discs[i];
      let gradient = this.mycontext.createRadialGradient(
        disc.x,
        disc.y,
        0,
        disc.x,
        disc.y,
        disc.radius,
      );
      gradient.addColorStop(0.3, disc.innerColor);
      gradient.addColorStop(0.5, disc.middleColor);
      gradient.addColorStop(1.0, disc.outerColor);

      this.mycontext.save();
      this.mycontext.beginPath();
      this.mycontext.arc(disc.x, disc.y, disc.radius, 0, Math.PI * 2, false);
      this.mycontext.fillStyle = gradient;
      this.mycontext.strokeStyle = disc.strokeStyle;
      this.mycontext.fill();
      this.mycontext.stroke();
      this.mycontext.restore();
    }
  };

  render = () => {
    return (
      <div>
        <div className={styles.controls}>
          <Button type="primary" onClick={this.handleAnimate}>
            {this.state.btn}
          </Button>
        </div>
        <canvas id="canvas" className={styles.canvas} width="750" height="500">
          Canvas not supported
        </canvas>
      </div>
    );
  };
}
