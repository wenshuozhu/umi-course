import React from 'react';
import styles from './example.css';

export default class Index extends React.Component {

  constructor(props) {
    super(props)

  }

  drawTwoArcs = () => {
    this.mycontext.fillStyle = 'rgba(100, 140, 230, 0.5)'
    this.mycontext.strokeStyle = this.mycontext.fillStyle

    this.mycontext.shadowColor = 'rgba(0,0,0,0.8)'
    this.mycontext.shadowOffsetX = 12
    this.mycontext.shadowOffsetY = 12
    this.mycontext.shadowBlur = 15

    this.mycontext.beginPath()
    this.mycontext.arc(300, 150, 150, 0, Math.PI * 2, false)
    this.mycontext.arc(300, 150, 100, 0, Math.PI * 2, true)
    this.mycontext.fill()

    this.mycontext.beginPath()
    this.mycontext.arc(300, 150, 150, 0, Math.PI * 2, false)
    this.mycontext.stroke()
    this.mycontext.beginPath()
    this.mycontext.arc(300, 150, 100, 0, Math.PI * 2, true)
    this.mycontext.stroke()

    this.mycontext.shadowColor = undefined
    this.mycontext.shadowOffsetX = 0
    this.mycontext.shadowOffsetY = 0
  }

  componentDidMount = () => {
    this.canvas = document.getElementById('canvas')
    this.mycontext = canvas.getContext('2d')
    // this.mycontext.font = '38pt Arial'
    // this.mycontext.fillStyle = 'cornflowerblue'
    // this.mycontext.strokeStyle = 'blue'
    // this.mycontext.fillText('Hello Canvas', this.canvas.width / 2 - 150, this.canvas.height / 2 + 15)
    // this.mycontext.strokeText('Hello Canvas', this.canvas.width / 2 - 150, this.canvas.height / 2 + 15)

    // this.mycontext.canvas.onmousedown = (e) => {
    //   this.mycontext.clearRect(0, 0, this.canvas.width, this.canvas.height)
    // }
    // this.drawTwoArcs()
    this.drawGrid('lightgray', 10, 10)
    this.drawAxes()
  }

  drawGrid = (color, stepx, stepy) => {
    this.mycontext.strokeStyle = color
    this.mycontext.lineWidth = 0.5
    for (let i = stepx + 0.5; i < this.mycontext.canvas.width; i += stepx) {
      this.mycontext.beginPath()
      this.mycontext.moveTo(i, 0)
      this.mycontext.lineTo(i, this.mycontext.canvas.height)
      this.mycontext.stroke()
    }
    for (let i = stepy + 0.5; i < this.mycontext.canvas.height; i += stepy) {
      this.mycontext.beginPath()
      this.mycontext.moveTo(0, i)
      this.mycontext.lineTo(this.mycontext.canvas.width, i)
      this.mycontext.stroke()
    }
  }

  drawAxes = () => {
    this.mycontext.save()
    const AXIS_MARGIN = 40
    this.AXIS_ORIGIN = { x: AXIS_MARGIN, y: this.canvas.height - AXIS_MARGIN }
    this.AXIS_TOP = AXIS_MARGIN
    this.AXIS_RIGHT = this.canvas.width - AXIS_MARGIN
    this.HORIZONTAL_TICK_SPACING = 10
    this.VERTICAL_TICK_SPACING = 10
    this.AXIS_WIDTH = this.AXIS_RIGHT - this.AXIS_ORIGIN.x
    this.AXIS_HEIGHT = this.AXIS_ORIGIN.y - this.AXIS_TOP
    this.NUM_VERTICAL_TICKS = this.AXIS_HEIGHT / this.VERTICAL_TICK_SPACING
    this.NUM_HORIZONTAL_TICKS = this.AXIS_WIDTH / this.HORIZONTAL_TICK_SPACING
    this.TICK_WIDTH = 10
    this.TICKS_LINEWIDTH = 0.5
    this.TICKS_COLOR = 'navy'
    this.AXIS_LINEWIDTH = 1
    this.AXIS_COLOR = 'blue'

    this.mycontext.strokeStyle = this.AXIS_COLOR
    this.mycontext.lineWidth = this.AXIS_LINEWIDTH
    this.drawHorizontalAxis()
    this.drawVerticalAxis()
    this.mycontext.lineWidth = this.TICKS_LINEWIDTH
    this.mycontext.strokeStyle = this.TICKS_COLOR
    this.drawVerticalAxisTicks()
    this.drawHorizontalAxisTicks()
    this.mycontext.restore()
  }

  drawHorizontalAxis = () => {
    this.mycontext.beginPath()
    this.mycontext.moveTo(this.AXIS_ORIGIN.x, this.AXIS_ORIGIN.y)
    this.mycontext.lineTo(this.AXIS_RIGHT, this.AXIS_ORIGIN.y)
    this.mycontext.stroke()
  }

  drawVerticalAxis = () => {
    this.mycontext.beginPath()
    this.mycontext.moveTo(this.AXIS_ORIGIN.x, this.AXIS_ORIGIN.y)
    this.mycontext.lineTo(this.AXIS_ORIGIN.x, this.AXIS_TOP)
    this.mycontext.stroke()
  }

  drawVerticalAxisTicks = () => {
    var deltaX
    for (let i = 1; i < this.NUM_VERTICAL_TICKS; i++) {
      this.mycontext.beginPath()
      if (i % 5 === 0) {
        deltaX = this.TICK_WIDTH
      }
      else {
        deltaX = this.TICK_WIDTH / 2
      }
      this.mycontext.moveTo(this.AXIS_ORIGIN.x - deltaX,
        this.AXIS_ORIGIN.y - i * this.VERTICAL_TICK_SPACING)
      this.mycontext.lineTo(this.AXIS_ORIGIN.x + deltaX,
        this.AXIS_ORIGIN.y - i * this.VERTICAL_TICK_SPACING)
      this.mycontext.stroke()
    }
  }

  drawHorizontalAxisTicks = () => {
    var deltaY
    for (let i = 1; i < this.NUM_HORIZONTAL_TICKS; i++) {
      this.mycontext.beginPath()
      if (i % 5 === 0) {
        deltaY = this.TICK_WIDTH
      }
      else {
        deltaY = this.TICK_WIDTH / 2
      }
      this.mycontext.moveTo(this.AXIS_ORIGIN.x + i * this.HORIZONTAL_TICK_SPACING,
        this.AXIS_ORIGIN.y - deltaY)
      this.mycontext.lineTo(this.AXIS_ORIGIN.x + i * this.HORIZONTAL_TICK_SPACING,
        this.AXIS_ORIGIN.y + deltaY)
      this.mycontext.stroke()
    }
  }

  render = () => {

    return (
      <div className={styles.body}>
        <h1 className={styles.title}>Page canvas/example</h1>

        <canvas id='canvas' className={styles.canvas} width='600' height='300'>
          Canvas not supported
        </canvas>

      </div>
    );
  }
}
