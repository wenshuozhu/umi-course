import React from 'react';
import styles from './example.css';

export default class Index extends React.Component {

  constructor(props) {
    super(props)
  }

  draw = () => {
    this.mycontext.clearRect(0, 0, this.mycontext.canvas.width, this.mycontext.canvas.height)
    this.mycontext.save()

    this.mycontext.shadowColor = 'rgba(200, 200, 0, 0.5)'
    this.mycontext.shadowOffsetX = 12
    this.mycontext.shadowOffsetY = 12
    this.mycontext.shadowBlur = 15

    this.drawCutouts()
    this.strokeCutoutShapes()

    this.mycontext.restore()
  }

  drawCutouts = () => {
    this.mycontext.beginPath()
    this.addOuterRectanglePath()
    this.addCirclePath()
    this.addRectanglePath()
    this.addTrianglePath()
    this.mycontext.fill()
  }

  strokeCutoutShapes = () => {
    this.mycontext.save()

    this.mycontext.strokeStyle = 'rgba(0, 0, 0, 0.7)'
    this.mycontext.beginPath()
    this.addOuterRectanglePath()
    this.mycontext.stroke()

    this.mycontext.beginPath()
    this.addCirclePath()
    this.addRectanglePath()
    this.addTrianglePath()
    this.mycontext.stroke()

    this.mycontext.restore()
  }

  addOuterRectanglePath = () => {
    this.rect(110, 25, 370, 335)
  }

  addCirclePath = () => {
    this.mycontext.arc(300, 300, 40, 0, Math.PI * 2, true)
  }

  addRectanglePath = () => {
    this.rect(310, 55, 70, 35, true)
  }

  /**
   * 
   * @param {*} x 
   * @param {*} y 
   * @param {*} w 
   * @param {*} h 
   * @param {*} direction 1: 逆时针；2：顺时针
   */
  rect = (x, y, w, h, direction) => {
    if (direction) {
      this.mycontext.moveTo(x, y)
      this.mycontext.lineTo(x, y + h)
      this.mycontext.lineTo(x + w, y + h)
      this.mycontext.lineTo(x + w, y)
      this.mycontext.closePath()
    }
    else {
      this.mycontext.moveTo(x, y)
      this.mycontext.lineTo(x + w, y)
      this.mycontext.lineTo(x + w, y + h)
      this.mycontext.lineTo(x, y + h)
      this.mycontext.closePath()
    }
  }

  addTrianglePath = () => {
    this.mycontext.moveTo(400, 200)
    this.mycontext.lineTo(250, 115)
    this.mycontext.lineTo(200, 200)
    this.mycontext.closePath()
  }

  componentDidMount = () => {
    this.canvas = document.getElementById('canvas')
    this.mycontext = canvas.getContext('2d')

    this.mycontext.fillStyle = 'goldenrod'
    this.draw()
  }

  render = () => {

    return (
      <div className={styles.body}>
        <h1 className={styles.title}>Page canvas/example</h1>

        <canvas id='canvas' className={styles.canvas} width='600' height='600'>
          Canvas not supported
        </canvas>

      </div>
    );
  }
}
