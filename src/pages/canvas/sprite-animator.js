import React from 'react';
import styles from './template.css';
import RequestNextAnimationFrame from '@/utils/requestNextAnimationFrame'
import SpriteAnimator from '@/utils/SpriteAnimator'
import ImagePainter from '@/utils/ImagePainter'
import Sprite from '@/utils/Sprite'
import { Button } from 'antd'

const BOMB_LEFT = 100
const BOMB_TOP = 80
const BOMB_WIDTH = 180
const BOMB_HEIGHT = 130

const NUM_EXPLOSION_PAINTERS = 9
const NUM_FUSE_PAINTERS = 9

export default class Index extends React.Component {

  constructor(props) {
    super(props)
  }

  componentDidMount = () => {
    this.canvas = document.getElementById('canvas')
    this.mycontext = this.canvas.getContext('2d')

    this.bombPainter = new ImagePainter(require('@/asset/bomb.png'))
    this.bombNoFusePainter = new ImagePainter(require('@/asset/bomb-no-fuse.png'))
    this.fuseBurningPainters = []
    this.explosionPainters = []

    this.bomb = new Sprite('bomb', this.bombPainter)
    this.bomb.left = BOMB_LEFT
    this.bomb.top = BOMB_TOP
    this.bomb.width = BOMB_WIDTH
    this.bomb.height = BOMB_HEIGHT
    for (let i = 0; i < NUM_FUSE_PAINTERS; i++) {
      this.fuseBurningPainters.push(new ImagePainter(require('@/asset/fuse/fuse-0' + i + '.png')))
    }
    for (let i = 0; i < NUM_EXPLOSION_PAINTERS; i++) {
      this.explosionPainters.push(new ImagePainter(require('@/asset/explosion/explosion-0' + i + '.png')))
    }

    this.fuseBurningAnimator = new SpriteAnimator(this.fuseBurningPainters,
      () => {
        this.bomb.painter = this.bombNoFusePainter
      }
    )
    this.explosionAnimator = new SpriteAnimator(this.explosionPainters,
      () => {
        this.bomb.painter = this.bombNoFusePainter
      })

    RequestNextAnimationFrame(this.animate)
  }

  resetBombNoFuse = () => {
    this.bomb.painter = this.bombNoFusePainter
  }

  animate = (now) => {
    console.log('main process')
    this.mycontext.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.bomb.paint(this.mycontext)
    // console.log('start paint one frame')
    RequestNextAnimationFrame(this.animate)
  }

  handleClick = () => {
    if (this.bomb.animating) {
      return
    }
    this.fuseBurningAnimator.start(this.bomb, 2000)
    // this.fuseBurningAnimator.start(this.bomb, 2000, this.mycontext)
    setTimeout(() => {
      this.explosionAnimator.start(this.bomb, 1000)
      // this.explosionAnimator.start(this.bomb, 1000, this.mycontext)
      setTimeout(() => {
        this.bomb.painter = this.bombPainter
      }, 2000)
    }, 3000)
  }

  render = () => {
    return (
      <div>
        <h1 className={styles.title}>Page canvas/template</h1>
        <div>
          <Button onClick={this.handleClick}>kaboom</Button>
        </div>
        <canvas id='canvas' width='365' height='275'></canvas>
      </div>
    );
  }
}
