import React from 'react';
import styles from './example.css';

export default class Index extends React.Component {

  constructor(props) {
    super(props)
  }

  drawCircle = () => {
    this.mycontext.beginPath()
    this.mycontext.arc(this.canvas.width / 2, this.canvas.height / 2, this.RADIUS, 0, Math.PI * 2, true)
    this.mycontext.stroke()
  }

  drawNumerals() {
    const numerals = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    var angle = 0
    var numeralWidth = 0

    numerals.forEach((numeral) => {
      angle = Math.PI / 6 * (numeral - 3)
      numeralWidth = this.mycontext.measureText(numeral).width
      this.mycontext.fillText(numeral,
        canvas.width / 2 + Math.cos(angle) * (this.HAND_RADIUS) - numeralWidth / 2,
        canvas.height / 2 + Math.sin(angle) * (this.HAND_RADIUS) + this.FONT_HEIGHT / 3)
    })
  }

  drawCenter = () => {
    this.mycontext.beginPath()
    this.mycontext.arc(this.canvas.width / 2, this.canvas.height / 2, 5, 0, Math.PI * 2, true)
    this.mycontext.fill()
  }

  drawClock = () => {
    this.mycontext.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.drawCircle()
    this.drawNumerals()
    this.drawCenter()
    this.drawHands()
  }

  drawHand = (loc, isHour) => {
    var angle = (Math.PI*2) * (loc/60)-Math.PI/2
    const handRadius = isHour ? this.RADIUS - this.HAND_TRUNCATION-this.HOUR_HAND_TRUNCATION
    : this.RADIUS - this.HAND_TRUNCATION
    this.mycontext.moveTo(this.canvas.width/2,this.canvas.height/2)
    this.mycontext.lineTo(this.canvas.width/2+ Math.cos(angle)*handRadius, this.canvas.height/2+Math.sin(angle)*handRadius)
    this.mycontext.stroke()
  }

  drawHands = () => {
    var date = new Date
    var hour = date.getHours()
    hour = hour > 12 ? hour - 12 : hour
    this.drawHand(hour * 5 + (date.getMinutes() / 60) * 5, true, 0.5)
    this.drawHand(date.getMinutes(), false, 0.5)
    this.drawHand(date.getSeconds(), false, 0.2)
  }

  componentDidMount = () => {
    this.canvas = document.getElementById('canvas')
    this.mycontext = canvas.getContext('2d')
    // console.log('context: ' + typeof(this.context.props))
    this.FONT_HEIGHT = 15
    this.MARGIN = 35
    this.HAND_TRUNCATION = this.canvas.width / 25
    this.HOUR_HAND_TRUNCATION = this.canvas.width / 10
    this.NUMERAL_SPACING = 20
    this.RADIUS = this.canvas.width / 2 - this.MARGIN
    this.HAND_RADIUS = this.RADIUS + this.NUMERAL_SPACING

    this.mycontext.font = this.FONT_HEIGHT + 'px Arial'
    setInterval(this.drawClock, 1000)
  }



  render = () => {

    return (
      <div className={styles.body}>
        <h1 className={styles.title}>Page canvas/example</h1>

        <canvas id='canvas' className={styles.canvas} width='300' height='300'>
          Canvas not supported
        </canvas>

      </div>
    );
  }
}
