import React from 'react';
import styles from './template.css';

class Sprite {
  constructor(name, painter, behaviors) {
    if (name !== undefined) {
      this.name = name
    }
    if (painter !== undefined) {
      this.painter = painter
    }
    this.top = 0
    this.left = 0
    this.width = 0
    this.height = 0
    this.velocityX = 0
    this.velocityY = 0
    this.visible = true
    this.animating = false
    this.behaviors = behaviors || []
  }

  paint = (context) => {
    if (this.painter !== undefined && this.visible) {
      this.painter.paint(this, context)
    }
  }

  update = (context, time) => {
    for (let i = 0; i < this.behaviors.length; i++) {
      this.behaviors[i].execute(this, context, time)
    }
  }
}

class ImagePainter{
  constructor(imageUrl){
    this.image = new Image()
    this.image.src = imageUrl
    // console.log(this.image.src)
  }

  paint = (sprite, context)=>{
    if(this.image.complete){
      context.drawImage(this.image, sprite.left, sprite.top, sprite.width, sprite.height)
    }
  }
}

export default class Index extends React.Component{

  construct(props) {
    this.super(props)
  }

  componentDidMount = ()=>{
    this.canvas = document.getElementById('canvas')
    this.mycontext = this.canvas.getContext('2d')

    this.bomb = new Sprite('bomb', new ImagePainter(require('@/asset/bomb.png')))
    this.bomb.left = 220
    this.bomb.top = 80
    this.bomb.width = 180
    this.bomb.height = 130
    window.requestAnimationFrame(this.animate)    
  }

  animate = ()=>{
    this.mycontext.clearRect(0,0,this.mycontext.width,this.mycontext.height)
    this.bomb.paint(this.mycontext)
    window.requestAnimationFrame(this.animate)
  }

  render = () => {
    return (
      <div>
        <h1 className={styles.title}>Page canvas/template</h1>
        <canvas id='canvas' width='600' height='600'></canvas>
      </div>
    );
  }
}
