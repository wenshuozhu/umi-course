import React from 'react';
import styles from './template.css';
import { Button } from 'antd'

class Sprite {
  constructor(name, painter, behaviors) {
    if (name !== undefined) {
      this.name = name
    }
    if (painter !== undefined) {
      this.painter = painter
    }
    this.top = 0
    this.left = 0
    this.width = 0
    this.height = 0
    this.velocityX = 0
    this.velocityY = 0
    this.visible = true
    this.animating = false
    this.behaviors = behaviors || []
  }

  paint = (context) => {
    if (this.painter !== undefined && this.visible) {
      this.painter.paint(this, context)
    }
  }

  update = (context, time) => {
    for (let i = 0; i < this.behaviors.length; i++) {
      this.behaviors[i].execute(this, context, time)
    }
  }
}

class SpriteSheetPainter {
  constructor(cells, spritesheet) {
    this.cells = cells || []
    this.cellIndex = 0
    this.spritesheet = spritesheet
  }

  advance = () => {
    if (this.cellIndex == this.cells.length - 1) {
      this.cellIndex = 0
    } else {
      this.cellIndex++
    }
  }

  paint = (sprite, context) => {
    const cell = this.cells[this.cellIndex]
    context.drawImage(this.spritesheet, cell.left, cell.top, cell.width, cell.height, sprite.left, sprite.top, cell.width, cell.height)
  }
}

export default class Index extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      btn: 'Animate',      
    }
  }

  componentDidMount = () => {
    this.canvas = document.getElementById('canvas')
    this.mycontext = this.canvas.getContext('2d')

    this.spritesheet = new Image()
    this.runnerCells = [
      {left: 0, top: 0, width: 47, height: 64},
      {left: 55, top: 0, width: 44, height: 64},
      {left: 107, top: 0, width: 39, height: 64},
      {left: 150, top: 0, width: 46, height: 64},
      {left: 208, top: 0, width: 49, height: 64},
      {left: 265, top: 0, width: 46, height: 64},
      {left: 320, top: 0, width: 42, height: 64},
      {left: 380, top: 0, width: 35, height: 64},
      {left: 425, top: 0, width: 35, height: 64},      
    ]

    let runInPlace = {
      lastAdvance: 0,
      PAGEFLIP_INTERVAL: 100,
      execute: function(sprite, context, now){
        // console.log(this.lastAdvance === NaN ? 'OK' : 'NO')
        // console.log(now-this.lastAdvance+' : '+this.PAGEFLIP_INTERVAL)
        if(now - this.lastAdvance > this.PAGEFLIP_INTERVAL){
          sprite.painter.advance()
          this.lastAdvance = now
        }
      }
    }
    
    this.moveLeftToRight = {
      lastMove: 0,
      canvas: this.canvas,
      execute: function(sprite, context, time){
        // console.log(this.lastMove)
        if(this.lastMove !== 0){
          sprite.left -= sprite.velocity
          X * ((time-this.lastMove)/1000)
          if(sprite.left < 0){
            sprite.left = canvas.width
          }
        }
        this.lastMove = time
      }
    }

    this.sprite = new Sprite('runner', new SpriteSheetPainter(this.runnerCells, this.spritesheet), [runInPlace, this.moveLeftToRight])
    this.interval
    // this.lastAdvance = 0
    this.paused = false
    // this.PAGEFLIP_INTERVAL = 100

    this.spritesheet.src = require('@/asset/running-sprite-sheet.png')
    this.spritesheet.onload = ()=>{
      this.mycontext.drawImage(this.spritesheet,0,0)
    }
    this.sprite.velocityX = 50
    this.sprite.left = 200
    this.sprite.top = 100
    this.mycontext.strokeStyle = 'lightgray'
    this.mycontext.lineWidth = 0.5
    this.drawBackground()
  }



  animateHandle = () => {
    if (this.state.btn === 'Animate') {
      // console.log('animate')
      this.startAnimation()
    } else {
      this.pauseAnimation()
    }
  }

  startAnimation = () => {
    this.setState({
      btn: 'Pause',
    })
    this.paused = false
    // this.lastAdvance = +new Date()
    window.webkitRequestAnimationFrame(this.animate)    
  }

  pauseAnimation = () => {
    this.setState({
      btn: 'Animate',      
    })
    this.paused = true
    this.moveLeftToRight.lastMove = 0
  }

  animate = (time)=>{    
    // console.log('run: '+time)
    if(!this.paused){
      this.mycontext.clearRect(0,0,this.canvas.width,this.canvas.height)
      this.drawBackground()
      this.mycontext.drawImage(this.spritesheet,0,0)
      this.sprite.update(this.mycontext, time)
      this.sprite.paint(this.mycontext)
      // if(time-this.lastAdvance > this.PAGEFLIP_INTERVAL){        
      //   this.sprite.painter.advance()
      //   this.lastAdvance = time
      // }
      window.webkitRequestAnimationFrame(this.animate)
    }
  }

  drawBackground = ()=>{
    const STEP_Y = 12
    let i = this.mycontext.canvas.height
    while(i > STEP_Y*4){
      this.mycontext.beginPath()
      this.mycontext.moveTo(0,i)
      this.mycontext.lineTo(this.mycontext.canvas.width, i)
      this.mycontext.stroke()
      i -= STEP_Y
    }
  }

  render = () => {        
    return (
      <div>
        <h1 className={styles.title}>Page canvas/template</h1>
        <div>
          <Button type='primary' onClick={this.animateHandle}>{this.state.btn}</Button>
        </div>
        <canvas id='canvas' width='600' height='600'></canvas>
      </div>
    );
  }
}
