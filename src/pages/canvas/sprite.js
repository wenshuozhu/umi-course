import React from 'react';
import styles from './sprite.css';


class Sprite {
  constructor(name, painter, behaviors) {
    if (name !== undefined) {
      this.name = name
    }
    if (painter !== undefined) {
      this.painter = painter
    }
    this.top = 0
    this.left = 0
    this.width = 0
    this.height = 0
    this.velocityX = 0
    this.velocityY = 0
    this.visible = true
    this.animating = false
    this.behaviors = behaviors || []
  }

  paint = (context) => {
    if (this.painter !== undefined && this.visible) {
      this.painter.paint(this, context)
    }
  }

  update = (context, time) => {
    for (let i = 0; i < this.behaviors.length; i++) {
      this.behaviors[i].execute(this, context, time)
    }
  }
}

export default class Index extends React.Component {

  constructor(props) {
    super(props)
  }

  drawGrid = (color, stepx, stepy) => {
    this.mycontext.strokeStyle = color
    this.mycontext.lineWidth = 0.5
    for (let i = stepx + 0.5; i < this.mycontext.canvas.width; i += stepx) {
      this.mycontext.beginPath()
      this.mycontext.moveTo(i, 0)
      this.mycontext.lineTo(i, this.mycontext.canvas.height)
      this.mycontext.stroke()
    }
    for (let i = stepy + 0.5; i < this.mycontext.canvas.height; i += stepy) {
      this.mycontext.beginPath()
      this.mycontext.moveTo(0, i)
      this.mycontext.lineTo(this.mycontext.canvas.width, i)
      this.mycontext.stroke()
    }
  }

  componentDidMount = () => {
    this.canvas = document.getElementById('canvas')
    this.mycontext = this.canvas.getContext('2d')

    this.RADIUS = 75
    this.ball = new Sprite('ball',
      {
        paint: (sprite, context) => {
          context.beginPath()
          context.arc(sprite.left + sprite.width / 2,
            sprite.top + sprite.height / 2,
            this.RADIUS, 0, Math.PI * 2, false)
          context.clip()
          context.shadowColor = 'rgb(0,0,0)'
          context.shadowOffsetX = -4
          context.shadowOffsetY = -4
          context.shadowBlur = 8
          context.lineWidth = 2
          context.strokeStyle = 'rgb(100,100,195)'
          context.fillStyle = 'rgba(30,144,255,0.15)'
          context.fill()
          context.stroke()
        }
      })
    this.drawGrid('lightgray', 10, 10)
    this.ball.left = 320
    this.ball.top = 160
    this.ball.paint(this.mycontext)
  }

  render = () => {
    return (
      <div>
        <h1 className={styles.title}>Page canvas/sprite</h1>
        <canvas id='canvas' width='600' height='600'></canvas>
      </div>
    );
  }
}
