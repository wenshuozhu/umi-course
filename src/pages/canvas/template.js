import React from 'react';
import styles from './template.css';

export default class Index extends React.Component{

  constructor(props) {
    super(props)
  }

  componentDidMount = ()=>{
    this.canvas = document.getElementById('canvas')
    this.mycontext = this.canvas.getContext('2d')
  }

  render = () => {
    return (
      <div>
        <h1 className={styles.title}>Page canvas/template</h1>
        <canvas id='canvas' width='600' height='600'></canvas>
      </div>
    );
  }
}
