import React from 'react';
import styles from './sprite-clock.css';


class Sprite {
  constructor(name, painter, behaviors) {
    if (name !== undefined) {
      this.name = name
    }
    if (painter !== undefined) {
      this.painter = painter
    }
    this.top = 0
    this.left = 0
    this.width = 0
    this.height = 0
    this.velocityX = 0
    this.velocityY = 0
    this.visible = true
    this.animating = false
    this.behaviors = behaviors || []
  }

  paint = (context) => {
    if (this.painter !== undefined && this.visible) {
      this.painter.paint(this, context)
    }
  }

  update = (context, time) => {
    for (let i = 0; i < this.behaviors.length; i++) {
      this.behaviors[i].execute(this, context, time)
    }
  }
}

export default class Index extends React.Component {

  constructor(props) {
    super(props)
  }

  drawGrid = (color, stepx, stepy) => {
    this.mycontext.strokeStyle = color
    this.mycontext.lineWidth = 0.5
    for (let i = stepx + 0.5; i < this.mycontext.canvas.width; i += stepx) {
      this.mycontext.beginPath()
      this.mycontext.moveTo(i, 0)
      this.mycontext.lineTo(i, this.mycontext.canvas.height)
      this.mycontext.stroke()
    }
    for (let i = stepy + 0.5; i < this.mycontext.canvas.height; i += stepy) {
      this.mycontext.beginPath()
      this.mycontext.moveTo(0, i)
      this.mycontext.lineTo(this.mycontext.canvas.width, i)
      this.mycontext.stroke()
    }
  }

  componentDidMount = () => {
    this.canvas = document.getElementById('canvas')
    this.mycontext = this.canvas.getContext('2d')

    this.CLOCK_RADIUS = this.canvas.width / 2 - 50
    this.HOUR_HAND_TRUNCATION = 75
    this.MINUES_HAND_TRUNCATION = 25
    this.ball = new Sprite('ball',
      {
        paint: (sprite, context) => {
          const radius = sprite.width / 2
          context.save()
          context.beginPath()
          context.arc(sprite.left + sprite.width / 2,
            sprite.top + sprite.height / 2,
            radius, 0, Math.PI * 2, false)
          context.clip()
          context.shadowColor = 'rgb(0,0,0)'
          context.shadowOffsetX = -4
          context.shadowOffsetY = -4
          context.shadowBlur = 8
          context.lineWidth = 2
          context.strokeStyle = 'rgb(100,100,195)'
          context.fillStyle = 'rgba(218,165,32,0.1)'
          context.fill()
          context.stroke()
          context.restore()
        }
      })

    this.mycontext.lineWidth = 0.5
    // this.mycontext.strokeStyle = 'rgba(0,0,0,0.2)'
    // this.mycontext.shadowColor = 'rgba(0,0,0,0.5)'
    // this.mycontext.shadowOffsetX = 2
    // this.mycontext.shadowOffsetY = 2
    // this.mycontext.shadowBlur = 4
    // this.mycontext.stroke()
    this.FONT_HEIGHT = 15
    this.NUMERAL_SPACING = 20
    this.mycontext.font = this.FONT_HEIGHT + 'px Arial'
    this.HAND_RADIUS = this.CLOCK_RADIUS + this.NUMERAL_SPACING
    window.requestAnimationFrame(this.animate)
    // this.drawGrid('lightgray', 10, 10)
  }

  drawNumerals = ()=> {
    const numerals = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    var angle = 0
    var numeralWidth = 0

    numerals.forEach((numeral) => {
      angle = Math.PI / 6 * (numeral - 3)
      numeralWidth = this.mycontext.measureText(numeral).width
      this.mycontext.fillText(numeral,
        this.canvas.width / 2 + Math.cos(angle) * (this.HAND_RADIUS) - numeralWidth / 2,
        this.canvas.height / 2 + Math.sin(angle) * (this.HAND_RADIUS) + this.FONT_HEIGHT / 3)
    })
  }

  animate = () => {
    this.mycontext.clearRect(0, 0, this.canvas.width, this.canvas.height)
    // this.drawGrid('lightgray', 10, 10)
    this.drawClock()
    window.requestAnimationFrame(this.animate)
  }

  drawClock = () => {
    this.drawClockFace()
    this.drawNumerals()
    this.drawHands()
  }

  drawClockFace = () => {
    this.mycontext.save()
    this.mycontext.beginPath()
    this.mycontext.arc(
      this.canvas.width / 2,
      this.canvas.height / 2,
      this.CLOCK_RADIUS,
      0,
      Math.PI * 2,
      false
    )
    this.mycontext.strokeStyle = 'rgba(0,0,0,0.2)'
    this.mycontext.stroke()
    this.mycontext.restore()
  }

  drawHands = () => {
    const date = new Date()
    var hour = date.getHours()
    hour = hour > 12 ? hour - 12 : hour
    this.ball.width = 20
    this.ball.height = 20
    this.drawHand(date.getSeconds(), false, false)
    this.ball.width = 35
    this.ball.height = 35
    this.drawHand(date.getMinutes(), false, true)
    this.ball.width = 50
    this.ball.height = 50
    this.drawHand(hour * 5 + (date.getMinutes() / 60) * 5, true, false)
    this.ball.width = 10
    this.ball.height = 10
    this.ball.left = this.canvas.width / 2 - this.ball.width / 2
    this.ball.top = this.canvas.height / 2 - this.ball.height / 2
    this.ball.paint(this.mycontext)

  }
  drawHand = (loc, isHour, isMinues) => {
    const angle = (Math.PI*2) * (loc/60) - Math.PI/2
    const handRadius = isHour ? this.CLOCK_RADIUS - this.HOUR_HAND_TRUNCATION : 
      (isMinues ? this.CLOCK_RADIUS - this.MINUES_HAND_TRUNCATION : this.CLOCK_RADIUS)
    const lineEnd = {
      x: this.canvas.width/2+Math.cos(angle)*(handRadius-this.ball.width/2),
      y: this.canvas.height/2+Math.sin(angle)*(handRadius-this.ball.height/2)
    }
    this.mycontext.beginPath()
    this.mycontext.moveTo(this.canvas.width/2,this.canvas.height/2)
    this.mycontext.lineTo(lineEnd.x, lineEnd.y)
    this.mycontext.stroke()
    this.ball.left = this.canvas.width/2 + Math.cos(angle)*handRadius-this.ball.width/2
    this.ball.top = this.canvas.height/2 + Math.sin(angle)*handRadius-this.ball.height/2
    this.ball.paint(this.mycontext)
  }
  render = () => {
    return (
      <div style={{textAlign: "center"}}>
        <h1 className={styles.title}>Page canvas/sprite</h1>
        <canvas id='canvas' width='600' height='600'></canvas>
      </div>
    );
  }
}
