import React from 'react';
import { Layout, Menu, Dropdown, Button } from 'antd';
import styles from './index.less';
import { Link } from 'umi';
import { DownOutlined, HomeOutlined } from '@ant-design/icons';

const { Header, Content, Footer } = Layout;
const { SubMenu, Item } = Menu;

//#region
//#endregion

// const menuData = [
//     // {route: '/hero', name: '英雄'},
//     // {route: '/item', name: '局内道具'},
//     // {route: '/summoner', name: '召唤师技能'}
//     { route: '/dromlife', name: '宿舍生活' }
// ]

// const menu_engineer_ethics = (
//     <Menu>
//         <Menu.Item key="1">
//             <Link to="engineer-ethics">作业一</Link>
//         </Menu.Item>
//         <Menu.Item>
//             <Link to="">待开发</Link>
//         </Menu.Item>
//     </Menu>
// )

// const menu_dromlife = (
//     <Menu>
//         <Menu.Item key="1">
//             <Link to="dromlife">常见信息</Link>
//         </Menu.Item>
//         <Menu.Item>
//             <Link to="">待开发</Link>
//         </Menu.Item>
//     </Menu>
// )

function BasicLayout(props) {
  // const location = props.location
  // const pathname = location.pathname
  const {
    location: { pathname },
    children,
  } = props;
  return (
    <Layout style={{ height: '100vh' }}>
      <Header>
        <div className={styles.logo}>
          <Link to={'/'}>
            <HomeOutlined />
          </Link>
          &nbsp;&nbsp;起风了
        </div>
        {
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={[pathname]}
            style={{ lineHeight: '64px' }}
          >
            {/* <Menu.Item key="/hero">
                        <Link to='hero'>英雄</Link>
                    </Menu.Item>
                    <Menu.Item key="/item">
                        <Link to="item">局内道具</Link>
                    </Menu.Item>
                    <Menu.Item key="/summoner">
                        <Link to="summoner">召唤师技能</Link>
                    </Menu.Item> */}
            {/* {
                        menuData.map(menu =>(
                            <Menu.Item key={menu.route}>
                                <Link to={menu.route}>{menu.name}</Link>
                            </Menu.Item>
                        ))
                    } */}
            <SubMenu title="工具">
              <Item key="numberEncode">
                <Link to="numberEncode">数字编码训练</Link>
              </Item>
              <Item key="">
                <Link to="">待开发</Link>
              </Item>
            </SubMenu>

            <SubMenu title="宿舍生活">
              <Item key="dromlife">
                <Link to="dromlife">常见信息</Link>
              </Item>
              <Item key="">
                <Link to="">待开发</Link>
              </Item>
            </SubMenu>

            <SubMenu title="工程伦理">
              <Item key="engineer-ethics">
                <Link to="engineer-ethics">作业一</Link>
              </Item>
              <Item key="2">
                <Link to="">待开发</Link>
              </Item>
            </SubMenu>

            {/* <Dropdown overlay={menu_dromlife}>
                        <Button>
                        宿舍生活<DownOutlined />
                        </Button>
                    </Dropdown>

                    <Dropdown overlay={menu_engineer_ethics}>
                        <Button>
                            工程伦理<DownOutlined />
                        </Button>
                    </Dropdown> */}
          </Menu>
        }
      </Header>
      <Content style={{ padding: '0 50px', marginTop: '50px' }}>
        <div style={{ background: '#fff', padding: 24, height: '100%' }}>
          {children}
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>起风了 Created By Mike</Footer>
    </Layout>
  );
}

export default BasicLayout;
