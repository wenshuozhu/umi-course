import RequestNextAnimationFrame from '@/utils/requestNextAnimationFrame'

export default class SpriteAnimator {
    constructor(painters, elapsedCallback) {
        this.painters = painters || []
        this.elapsedCallback = elapsedCallback
        this.duration = 1000
        this.startTime = 0
        this.index = 0
    }

    end = (sprite, originalPainter) => {
        sprite.animating = false
        if (this.elapsedCallback) {
            this.elapsedCallback(sprite)
        } else {
            sprite.painter = originalPainter
        }
    }

    start = (sprite, duration) => {
        // console.log(+new Date())
        // console.log(duration)
        const endTime = +new Date() + duration
        const period = duration / (this.painters.length)
        const originalPainter = sprite.painter
        // const animator = this
        // let lastUpdate = 0

        this.index = 0
        sprite.animating = true
        sprite.painter = this.painters[this.index]
        // RequestNextAnimationFrame(function spriteAnimatorAnimate(time) {                  
        //     if (time < endTime) {         
        //         // console.log(time+' : '+endTime)                       
        //         if ((time - lastUpdate) > period) {
        //             console.log(sprite.name+' 换帧')
        //             // console.log('当前帧图片 '+sprite.painter.image ? sprite.painter.image.src : 'NULL')
        //             sprite.painter = animator.painters[++animator.index]     
        //             // context.clearRect(0, 0, context.canvas.width, context.canvas.height)
        //             // sprite.paint(context)                                   
        //             lastUpdate = time
        //         }
        //         RequestNextAnimationFrame(spriteAnimatorAnimate)
        //     } else {
        //         animator.end(sprite, originalPainter)
        //     }
        // })
        console.log('child process')
        let interval = setInterval(() => {
            if (+new Date() < endTime) {
                sprite.painter = this.painters[++this.index]
            } else {
                this.end(sprite, originalPainter)
                clearInterval(interval)
            }
        }, period)
    }
}