export default window.requestNextAnimationFrame = (
    function(){
        var self = this,
        originalWebkitMethod,
        wrapper = undefined,
        callback = undefined,
        index = 0,
        userAgent = navigator.userAgent,
        geckoVersion = 0
        // callback = undefined

        console.log('requestNextAnimationFrame')
        if(window.webkitRequestAnimationFrame){
            console.log('webkit')
            wrapper = function(time){
                if(time === undefined){
                    time = +new Date()
                }
                callback(time)
            }
            originalWebkitMethod = window.webkitRequestAnimationFrame
            window.webkitRequestAnimationFrame = function(cb, element){
                callback = cb
                originalWebkitMethod(wrapper, element)
            }
        }

        if(window.mozRequestAnimationFrame){
            index = userAgent.indexOf('rv:')
            if(userAgent.indexOf('Gecko') != -1){
                geckoVersion = userAgent.substr(index+3,3)
                if(geckoVersion == '2.0'){
                    window.mozRequestAnimationFrame = undefined
                }
            }    
        }

        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback, element){
            var start, finish
            window.setTimeout(function(){
                start = +new Date()
                callback(start)
                finish = +new Date()
                self.timeout = 1000/60-(finish-start)
            }, self.timeout)
        }
    }
)()

// export default window.webkitRequestAnimationFrame