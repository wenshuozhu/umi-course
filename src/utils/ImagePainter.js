export default class ImagePainter{
    constructor(imageUrl){
      this.image = new Image()
      this.image.src = imageUrl
      // console.log(this.image.src)
    }
  
    paint = (sprite, context)=>{
      if(this.image.complete){
        context.drawImage(this.image, sprite.left, sprite.top, sprite.width, sprite.height)
      }
    }
  }