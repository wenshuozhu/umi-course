// import { defineConfig } from 'umi';

// export default defineConfig({
//   nodeModulesTransform: {
//     type: 'none',
//   },
//   routes: [
//     { path: '/', component: '@/pages/index' },
//   ],
// });

import { defineConfig } from 'umi';
import pages from '@/pages';

export default defineConfig({
    dva: {},
    antd: {},   
    proxy: {
        '/api': {
            'target': 'https://pvp.qq.com',
            'changeOrigin': true,
            'pathRewrite': { '^/api': '' }
        }
    },
    base: '/umi-course/',
    publicPath: '/umi-course/',
    plugins: [
        // 'umi-plugin-gh-pages'
    ],

});

