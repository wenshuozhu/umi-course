import herolist from './herolist.json'

export default {
    '/apimock/web201605/js/herolist.json': herolist,
    'POST /apimock/herodetails.json': (req, res) => {
        const { ename } = req.body;
        const hero = herolist.filter(item => item.ename === parseInt(ename, 10))[0];
        res.send(hero);
    },
};